<?php

namespace Modules\Order\Models;

//use App\Models\Tenant\Catalogs\IdentityDocumentType;
use App\Models\Tenant\ModelTenant;

class Asistencia extends ModelTenant
{

  //  protected $with = ['identity_document_type'];
 
    protected $fillable = [
        'nombres',
        'id_usuario',
        'fecha',
        'hora',
        'comentario',
        'tipo',       
        
    ];


   // public function identity_document_type()
   // {
       // return $this->belongsTo(IdentityDocumentType::class, 'identity_document_type_id');
   // }

}
