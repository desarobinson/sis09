<?php

namespace Modules\Order\Models;

//use App\Models\Tenant\Catalogs\IdentityDocumentType;
use App\Models\Tenant\ModelTenant;

class Carreta extends ModelTenant
{

  //  protected $with = ['identity_document_type'];
 
    protected $fillable = [
        'placa',
        'color',
        'modelo',
        'descripcion',
        
    ];


   // public function identity_document_type()
   // {
       // return $this->belongsTo(IdentityDocumentType::class, 'identity_document_type_id');
   // }

}
