@php
    $establishment = $document->establishment;
    $supplier = $document->supplier;
    
    $tittle =str_pad($document->id, 8, '0', STR_PAD_LEFT); 
@endphp
<html>
<head>
    {{--<title>{{ $tittle }}</title>--}}
    
    {{-- <style>
        .ticket {
            width: 283px;
            max-width: 280px;
        }
    </style>--}}
</head>
<body>
<div class="ticket">
<table class="full-width">
    <tr>
        @if($company->logo)
            <td width="20%">
                <div class="company_logo_box">
                    <img src="data:{{mime_content_type(public_path("storage/uploads/logos/{$company->logo}"))}};base64, {{base64_encode(file_get_contents(public_path("storage/uploads/logos/{$company->logo}")))}}" alt="{{$company->name}}" class="company_logo" style="max-width: 150px;">
                </div>
            </td>
        @else
            <td width="20%">
                {{--<img src="{{ asset('logo/logo.jpg') }}" class="company_logo" style="max-width: 150px">--}}
            </td>
        @endif
        <td width="50%" class="pl-3">
            <div class="text-left">
                <h4 class="">{{ $company->name }}</h4>
                <h5>{{ 'RUC '.$company->number }}</h5>
                
            </div>
        </td>
        <td width="30%" class="border-box py-4 px-2 text-center">
            <h5 class="text-center">GASTOS</h5>
            <h3 class="text-center">{{ $tittle }}</h3>
        </td>
    </tr>
</table>
<table class="full-width mt-5">    <tr>
        <td width="20%">Orden de trabajo:</td>
        <td width="15%">{{$document->order_id }}</td>
        <td width="15%">Fecha:</td>
        <td width="15%">{{ $document->created_at->format('Y-m-d') }}</td>
    
   
   
</table>
<table class="full-width mt-5">
    <tr>
        <td width="20%">Descripcion:</td>
        <td width="60%">{{ $document->descripcion}}</td>
        
    </tr>
   
    <tr>
        <td width="20%">Galones:</td>
        <td width="45%">{{ $document->galones}}</td>
        
    </tr>
    <tr>
        <td width="20%">Precio:</td>
        <td width="45%">{{ $document->galones}}</td>
        
    </tr>
    <tr>
        <td width="20%">Tipo:</td>
        <td width="45%">{{ $document->tipo}}</td>
        
    </tr>
    <tr>
        <td width="20%"><b>Monto:</b></td>
        <td width="45%"><b>{{ $document->monto}}<b></td>
        
    </tr>
  
</table>
<table class="full-width mt-5">
<tr>
         <td colspan="12" style="margin-left:60px"><b>________________________________________________</b></td>
         
     
 </tr>
 <tr>
        
         <td colspan="6" style="margin-left:60px"><b> FIRMA</b></td>
     
 </tr>
</table>


</div>
</body>
</html>
